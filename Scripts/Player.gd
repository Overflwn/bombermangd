extends KinematicBody2D
var bomb_scene = preload("res://Scenes/Bomb.tscn")
const bomb_class = preload("res://Scripts/Bomb.gd")
export(float) var move_speed = 60
export(float) var move_multiplier = 1

enum direction {UP, DOWN, LEFT, RIGHT}

# The last direction the player (tried) to move in
var last_movement = direction.DOWN

# Currently only set to false when dead.
# This is to play the dying animation without the player still moving around
var canMove = true

# Used to check if the player for example pressed A and D together, so that the
# player doesn't to weird movement.
var isMoving = false

# How many bombs are currently placed
var count_bombs = 0
export(int) var max_bombs = 1

export(bool) var can_kick = false

export(int) var bomb_range = 1

# This is used temporarily to update these values for the other clients who
# don't own this Player entity.
puppet var slave_pos = Vector2()
puppet var slave_direction = direction.DOWN
puppet var slave_isMoving = false

var game_node = null

func _ready():
	game_node = get_node("/root/Game")

func upgradeBombs():
	if max_bombs < 4:
		max_bombs += 1
	
func upgradeSpeed():
	if move_multiplier < 2:
		move_multiplier += 0.25
		
func upgradeKick():
	can_kick = true
	
func upgradeRange():
	if bomb_range < 6:
		bomb_range += 1
		
func _input(event):
	if is_network_master():
		if event.is_action_pressed("bomb"):
			if count_bombs < max_bombs:
				rpc("_place_bomb")
		if Input.is_key_pressed(KEY_B):
			rpc("_die")
		if Input.is_key_pressed(KEY_F):
			print(position)
			
# Place a bomb in front of the player, if there is no block in front of him
remotesync func _place_bomb():
	# just in case
	$RayCast2D.force_raycast_update()
	var ray_colliding = $RayCast2D.is_colliding()
	if not ray_colliding:
		var new_bomb = bomb_scene.instance()
		new_bomb.setCounter(bomb_range)
		get_parent().add_child(new_bomb)
		# snap to 16x16 grid; +8 because the sprite pivot point is in the center of the sprite
		var r = $RayCast2D.cast_to
		print(str(position) + " " + str(r) + " " + str(position+r))
		
		new_bomb.position.x = int(floor((position.x+r.x)/game_node.get_cell_width())) * game_node.get_cell_width() + 8
		new_bomb.position.y = int(floor((position.y+r.y)/game_node.get_cell_height())) * game_node.get_cell_height() + 8
		
		count_bombs += 1
		# Start a timer (corresponding to the detonation time of the bomb)
		# that, when finished, decrements the count_bombs value.
		var timer = Timer.new()
		add_child(timer)
		timer.connect("timeout", self, "_on_Timer_timeout", [timer])
		timer.set_one_shot(true)
		if timer.is_paused():
			timer.set_paused(false)
		timer.start(5)
	

func _physics_process(_delta):
	if not canMove:
		return
	
	# The owning client may move this Player
	if is_network_master():
		var hasChanged = false
		if Input.is_action_pressed("left"):
			last_movement = direction.LEFT
			hasChanged = true
		if Input.is_action_pressed("right"):
			last_movement = direction.RIGHT
			hasChanged = true
		if Input.is_action_pressed("up"):
			last_movement = direction.UP
			hasChanged = true
		if Input.is_action_pressed("down"):
			last_movement = direction.DOWN
			hasChanged = true
		isMoving = hasChanged
		# This value has to be updated on every non-owning client so that animations work
		# -> "slave"
		rset("slave_isMoving", isMoving)
		# This packet may be lost due to the position being updated next frame anyway.
		rset_unreliable("slave_pos", position)
		# The direction has to arrive, so that the sprite looks in the right direction.
		rset("slave_direction", last_movement)
		# Move the entity on your end.
		_move()
	else:
		# The owning client remotely set these values, copy them
		last_movement = slave_direction
		isMoving = slave_isMoving
		# Move the entity on your end
		_move()
		# However, still sync the position so that your won't desync over time
		position = slave_pos

# Explosions only call die() on the owning client
master func die():
	rpc("kill")

# Tell everybody to kill this player
remotesync func kill():
	canMove = false
	$AnimationPlayer.play("die")

func _move():
	if not canMove:
		return
	var oldpos = position
	var moving = Vector2()
	match last_movement:
		direction.UP:
			moving.y -= move_speed * move_multiplier
			$RayCast2D.cast_to = Vector2(0, -16)
		direction.DOWN:
			moving.y += move_speed * move_multiplier
			$RayCast2D.cast_to = Vector2(0, 16)
		direction.LEFT:
			moving.x -= move_speed * move_multiplier
			$RayCast2D.cast_to = Vector2(-16, 0)
		direction.RIGHT:
			moving.x += move_speed * move_multiplier
			$RayCast2D.cast_to = Vector2(16, 0)
	
	if isMoving:
# warning-ignore:return_value_discarded
		move_and_slide(moving)
		for i in range(get_slide_count()):
			var collision = get_slide_collision(i)
			# Kick the bomb if you can
			if collision.collider is bomb_class and can_kick:
				collision.collider.setVelocity(-collision.get_normal() * Vector2(250,250))
	
	# Play moving / idle animation
	match last_movement:
		direction.UP:
			if oldpos == position:
				$AnimationPlayer.play("idle_up")
			else:
				if not ($AnimationPlayer.current_animation == "move_up"):
					$AnimationPlayer.play("move_up")
		direction.DOWN:
			if oldpos == position:
				$AnimationPlayer.play("idle_down")
			else:
				if not ($AnimationPlayer.current_animation == "move_down"):
					$AnimationPlayer.play("move_down")
		direction.LEFT:
			if oldpos == position:
				$AnimationPlayer.play("idle_left")
			else:
				if not ($AnimationPlayer.current_animation == "move_left"):
					$AnimationPlayer.play("move_left")
		direction.RIGHT:
			if oldpos == position:
				$AnimationPlayer.play("idle_right")
			else:
				if not ($AnimationPlayer.current_animation == "move_right"):
					$AnimationPlayer.play("move_right")

# Destroy entity when the animation finishes
func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "die":
		queue_free()
		if is_network_master():
			# Tell the server that you died. (only if this client owns this entity)
			game_node.dead()

# Decrement count of currently placed bombs after a bomb detonates
func _on_Timer_timeout(timer):
	count_bombs -= 1
	timer.queue_free()
