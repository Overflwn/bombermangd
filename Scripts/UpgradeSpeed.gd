extends Area2D
const player_class = preload("res://Scripts/Player.gd")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_UpgradeSpeed_body_entered(body):
	if body is player_class:
		body.upgradeSpeed()
		queue_free()
