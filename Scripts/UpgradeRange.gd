extends Area2D
const player_class = preload("res://Scripts/Player.gd")

func _on_UpgradeRange_body_entered(body):
	if body is player_class:
		body.upgradeRange()
		queue_free()
