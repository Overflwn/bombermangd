extends Panel

var level_list = [
]
var current_level = -1
var empty = preload("res://Scenes/Level_empty.tscn")
var level = null

func _ready():
	# The preview is actually loading the levels live aswell.
	level = empty.instance()
	$LevelPreview.add_child(level)
	level.set_name("Level")
	var dir = Directory.new()
	# List every installed level
	if dir.open("user://Levels") == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if file_name.ends_with(".dat") and not dir.current_is_dir():
				level_list.append(file_name)
			file_name = dir.get_next()
	if len(level_list) > 0:
		current_level = 0
	lobbyPreview()
	$BackButton.disabled = true
	if (current_level + 1) == level_list.size():
		$NextButton.disabled = true

# Return the level array
func getSelectedLevel():
	if current_level == -1:
		return []
	else:
		return _getData(level_list[current_level])

# Read the JSON array from the file and interpret it as an array
func _getData(levelname):
	var file = File.new()
	file.open("user://Levels/"+levelname, File.READ)
	var content = file.get_as_text()
	file.close()
	return JSON.parse(content).get_result()


func lobbyPreview():
	if current_level >= 0:
		var level_name = level_list[current_level]
		var level_dat = _getData(level_name)
		level.loadData(level_dat)
		$Label.text = level_name
		if get_tree().is_network_server():
			# Tell the other clients to load up your preview
			rpc("preview", level_dat, level_name)
	var v = level.get_used_rect()
	# scale the preview to fit into the panel
	if v.size > Vector2(0,0):
		var dimensions = Vector2(v.size.x*level.cell_size.x, v.size.y*level.cell_size.y)
		var dim_panel = $LevelPreview.get_rect().size
		var scale_x = dim_panel.x / dimensions.x
		var scale_y = dim_panel.y / dimensions.y
		level.scale.x = scale_x
		level.scale.y = scale_y

# Send other clients your preview
remote func preview(data, level_name):
	level.loadData(data)
	$Label.text = level_name
	var v = level.get_used_rect()
	if v.size > Vector2(0,0):
		var dimensions = Vector2(v.size.x*level.cell_size.x, v.size.y*level.cell_size.y)
		var dim_panel = $LevelPreview.get_rect().size
		var scale_x = dim_panel.x / dimensions.x
		var scale_y = dim_panel.y / dimensions.y
		level.scale.x = scale_x
		level.scale.y = scale_y

func addPlayer(slot, name):
	match slot:
		1:
			$Slot1/Label.text = name
		2:
			$Slot2/Label.text = name
		3:
			$Slot3/Label.text = name
		4:
			$Slot4/Label.text = name

func _on_Slot2_kick():
	get_parent().kick(2)
	$Slot2/Label.text = "Waiting for player..."

func _on_Slot3_kick():
	get_parent().kick(3)
	$Slot3/Label.text = "Waiting for player..."

func _on_Slot4_kick():
	get_parent().kick(4)
	$Slot4/Label.text = "Waiting for player..."

# Start the whole game
func _on_PlayButton_pressed():
	get_parent().go()

func _on_BackButton_pressed():
	current_level -= 1
	$NextButton.disabled = false
	if current_level == 0:
		$BackButton.disabled = true
	lobbyPreview()


func _on_NextButton_pressed():
	current_level += 1
	$BackButton.disabled = false
	if (current_level + 1) == level_list.size():
		$NextButton.disabled = true
	lobbyPreview()
