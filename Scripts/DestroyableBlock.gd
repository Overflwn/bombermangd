extends StaticBody2D

# every upgrade in the game
const upgrades = [
	preload("res://Scenes/UpgradeBomb.tscn"),
	preload("res://Scenes/UpgradeSpeed.tscn"),
	preload("res://Scenes/UpgradeKick.tscn"),
	preload("res://Scenes/UpgradeRange.tscn")
]
var rng = RandomNumberGenerator.new()

func destroy():
	# server manages what item is dropped
	if get_tree().is_network_server():
		#TODO: drop items, etc
		rng.randomize()
		var spawn = rng.randi_range(0, 100)
		if spawn <= 40:
			# don't spawn anything
			rpc("_destroy_and_place", -1)
		else:
			rng.randomize()
			var i = rng.randi_range(0, len(upgrades)-1)
			rpc("_destroy_and_place", i)
	
remotesync func _destroy_and_place(upgrade):
	if upgrade > -1:
		var up = upgrades[upgrade].instance()
		up.position = position
		get_parent().call_deferred("add_child", up)
	queue_free()
