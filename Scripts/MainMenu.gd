extends Panel

var game_node = null

func _ready():
	game_node = get_node("/root/Game")

func _on_HostButton_pressed():
	game_node.createServer()

func _on_ConnectButton_pressed():
	if $IPBox.text != "":
		game_node.createClient($IPBox.text)

func _on_LevelEditorButton_pressed():
	$ConfirmationDialog.popup()

func _on_ConfirmationDialog_confirmed():
	game_node.loadEditor($ConfirmationDialog/Panel/Width.value, $ConfirmationDialog/Panel/Height.value)
