extends Node2D

var lobby_scene = preload("res://Scenes/Lobby.tscn")
var menu_scene = preload("res://Scenes/MainMenu.tscn")
var editor_scene = preload("res://Scenes/LevelEditor.tscn")
# Base level containing the tileset
var empty_scene = preload("res://Scenes/Level_empty.tscn")

var player_scene = preload("res://Scenes/Player.tscn")

var peer = NetworkedMultiplayerENet.new()
# All connected players
var players = {}
# Players done configuring
var players_done = []
# Players still alive
var players_alive = []
# Info we send to other players
var my_info = { name = "Server", num = 1 }
# Open player slots
var free_slots = [2,3,4]

# WIP: Global game value setting the overall level grid dimensions in px
export(float) var cell_width = 16
export(float) var cell_height = 16

func get_cell_width():
	return cell_width
	
func get_cell_height():
	return cell_height

# Connect every network message
func _ready():
# warning-ignore:return_value_discarded
	get_tree().connect("network_peer_connected", self, "_player_connected")
# warning-ignore:return_value_discarded
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
# warning-ignore:return_value_discarded
	get_tree().connect("connected_to_server", self, "_connected_ok")
# warning-ignore:return_value_discarded
	get_tree().connect("connection_failed", self, "_connected_fail")
# warning-ignore:return_value_discarded
	get_tree().connect("server_disconnected", self, "_server_disconnected")

# TODO: Remove?
func _player_connected(_id):
	# Called on both clients and server when a peer connects. Send my info to it.
	pass

# Called by the lobby scene, kick player (based on given slot) from the server 
# properly.
func kick(slot):
	for p in players:
		if players[p]["num"] == slot:
			get_tree().get_network_peer().disconnect_peer(p)
	
func _player_disconnected(id):
	print("Player " + str(id) + " disconnected.")
	
	# Open up the lobby slot once again so that another player can take his place.
	free_slots.push_back(players[id]["num"])
	if has_node("Lobby"):
		$Lobby.get_node("Slot"+str(players[id]["num"])).get_node("Label").text = "Waiting for player..."
	players.erase(id) # Erase player from info.
	# The next player gets into his place
	free_slots.sort()
	
	# If we are already ingame, kill the player like he would've died in an
	# explosion.
	if has_node("Level") and has_node("Level/"+str(id)):
		get_node("Level/"+str(id)).kill()
	if get_tree().is_network_server():
		players_alive.erase(id)
		print("Player " + str(id) + " died.")
		if players_alive.size() == 1:
			print("Player " + str(players_alive[0]) + " won!")
			rpc("game_over", players_alive[0])

func _connected_ok(): # Called only on clients
	print("Connected to server.")
	var peerID = get_tree().get_network_unique_id()
	players[peerID] = my_info
	_loadLobby()
	# Send server my info
	rpc_id(1, "register_player", peerID, my_info)

func _server_disconnected():
	print("Lul we got kicked. Okay dude.")
	loadMenu()
	# Server kicked us; show error and abort.

func _connected_fail():
	print("Failed to connect to server.")
	$MainMenu/HostButton.disabled = false
	$MainMenu/ConnectButton.disabled = false
	$MainMenu/Username.editable = true
	get_tree().set_network_peer(null)
	pass # Could not even connect to server; abort.

remote func register_player(id, info):
	# Store the info
	players[id] = info
	
	print("Registered player: " + info["name"])
	if get_tree().is_network_server():
		players[id]["num"] = free_slots.pop_front()
		# Display the player in the lobby scene
		$Lobby.addPlayer(players[id]["num"], players[id]["name"])
		
		# Send the new player the info from all the other players.
		for i in players:
			rpc_id(id, "register_player", i, players[i])
		# Send all the other players the info from this player.
		# NOTE: This sends the new player his own info, which doesn't matter
		# as it won't change anything.
		rpc("register_player", id, players[id])
	
	# Add the new player to the lobby scene
	$Lobby.addPlayer(players[id]["num"], players[id]["name"])
	
func go():
	if get_tree().is_network_server():
		print("All players are there.")
		# Stop new players from connecting.
		get_tree().set_refuse_new_network_connections(true)
		# Tell all players (and myself) to load up the selected level.
		# The full level data gets sent as a two-dimensional array.
		rpc("pre_configure_game", $Lobby.getSelectedLevel())

# Create the level live based on the given array.
func _loadWorld(level_data):
	var world = empty_scene.instance()
	world.set_name("Level")
	world.loadData(level_data)
	
	# Scale the map to fit into the screen.
	var v = world.get_used_rect()
	if v.size > Vector2(0,0):
		var dimensions = Vector2(v.size.x*world.cell_size.x, v.size.y*world.cell_size.y)
		var dim_window = Vector2(ProjectSettings.get_setting("display/window/size/width"),ProjectSettings.get_setting("display/window/size/height"))
		var scale_x = dim_window.x / dimensions.x
		var scale_y = dim_window.y / dimensions.y
		world.apply_scale(Vector2(scale_x,scale_y))
	else:
		# Shouldn't happen, remainder of early versions.
		world.apply_scale(Vector2(2,2))
	
	add_child(world)
	
# Get the spawn point for the specific player slot.
func _getPlayerPosition(level_data, num):
	var player = "player"+str(num)
	for y in len(level_data):
		for x in len(level_data[y]):
			if level_data[y][x]["type"] == player:
				return Vector2(x, y)
	return Vector2(-1,-1)

# Load up the given level and set everything up, then tell the server that you
# are ready to start the game.
# This is necessary as some clients may take longer than other and thus the game
# would technically de-sync.
remotesync func pre_configure_game(levelData):
	print("Pre-config")
	$Lobby.queue_free()
	# Pause the game (prevent all scripts from executing)
	get_tree().set_pause(true)
	print("Running pre-config")
	var selfPeerID = get_tree().get_network_unique_id()

	# Load the given array
	_loadWorld(levelData)
	# Load player entities
	print("Loading " + str(len(players)) + " players.")
	for p in players:
		var player = player_scene.instance()
		player.set_name(str(p))
		player.set_network_master(p) # The client has full control over this entity.
		var pos = _getPlayerPosition(levelData, players[p]["num"])
		if not (pos == Vector2(-1,-1)):
			player.position = Vector2(pos.x*cell_width+8, pos.y*cell_height+5)
		else:
			# The map has no spawn point for that player slot
			player.position = Vector2(cell_width+8, cell_height+5)
		get_node("/root/Game/Level").add_child(player)

	# Tell server (remember, server is always ID=1) that this peer is done pre-configuring.
	rpc_id(1, "done_preconfiguring", selfPeerID)

# A client has finished configuring
remotesync func done_preconfiguring(who):
	assert(get_tree().is_network_server())
	assert(who in players)
	assert(not who in players_done) # Was not added yet

	players_done.append(who)
	players_alive.append(who)
	
	if players_done.size() == players.size():
		print("All players are done configuring.")
		rpc("post_configure_game")

# Called by Player entity (only on the client owning that entity)
func dead():
	rpc_id(1, "player_died", get_tree().get_network_unique_id())

# A player died, kill him properly.
# This is only supposed to be executed on the server.
remotesync func player_died(who):
	players_alive.erase(who)
	print("Player " + str(who) + " died.")
	if players_alive.size() == 1:
		print("Player " + str(players_alive[0]) + " won!")
		rpc("game_over", players_alive[0])
	elif players_alive.size() == 0:
		print("Everybody died.")
		rpc("game_over", -1)

remotesync func game_over(winner):
	print("GAME OVER!!!!!!")
	get_node("Label").raise()
	if winner > -1:
		get_node("Label").text = "Player " + str(winner) + " won!"
	else:
		get_node("Label").text = "Tie!"
	get_tree().set_pause(true)

remotesync func post_configure_game():
	print("Post-config")
	get_tree().set_pause(false)
	# Game starts now!

# Connect to a host.
# Called by MainMenu upon pressing Connect button.
func createClient(ip):
	if $MainMenu/Username.text != "":
		$MainMenu/HostButton.disabled = true
		$MainMenu/ConnectButton.disabled = true
		$MainMenu/Username.editable = false
		my_info["name"] = $MainMenu/Username.text
		peer.create_client(ip,6969)
		get_tree().set_network_peer(peer)
		print("Client connecting to server.")

# Host a game.
# Called by MainMenu upon pressing Host button.
func createServer():
	if $MainMenu/Username.text != "":
		var name = $MainMenu/Username.text
		my_info["name"] = name
		peer.create_server(6969, 4)
		get_tree().set_network_peer(peer)
		_loadLobby(true, name)
		print("Server started.")
		players[1] = my_info

# Load up the level editor.
# Called by MainMenu upon pressing LevelEditor button.
func loadEditor(width, height):
	$MainMenu.queue_free()
	var editor = editor_scene.instance()
	editor.set_name("LevelEditor")
	add_child(editor)
	# Create a new map based on the size set in the dialog box.
	editor.initialize(width, height)

# Load the main menu back up.
# Called by either getting kicked from the server or exiting the LevelEditor.
func loadMenu():
	if has_node("Lobby"):
		$Lobby.queue_free()
	elif has_node("Level"):
		$Level.queue_free()
	elif has_node("LevelEditor"):
		$LevelEditor.queue_free()
	get_tree().set_network_peer(null)
	var mainmenu = menu_scene.instance()
	mainmenu.set_name("MainMenu")
	add_child(mainmenu)

# Load the lobby.
# Called when hosting or when successfully connecting to a host.
func _loadLobby(host=false,name=""):
	$MainMenu.queue_free()
	var lobby = lobby_scene.instance()
	lobby.set_name("Lobby")
	add_child(lobby)
	if not host:
		$Lobby/Slot2/Button.disabled = true
		$Lobby/Slot3/Button.disabled = true
		$Lobby/Slot4/Button.disabled = true
		$Lobby/PlayButton.disabled = true
		$Lobby/BackButton.disabled = true
		$Lobby/NextButton.disabled = true
	else:
		$Lobby.addPlayer(1, name)
