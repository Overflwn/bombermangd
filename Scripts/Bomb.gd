extends KinematicBody2D

var explosion_scene = load("res://Scenes/Explosion.tscn")
var cur_velocity = Vector2(0,0)
var counter = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("idle")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(_delta):
# warning-ignore:return_value_discarded
	move_and_slide(cur_velocity)
	if get_slide_count() > 0:
		cur_velocity = Vector2(0,0)

func _explode():
	var new_bomb = explosion_scene.instance()
	get_parent().add_child(new_bomb)
	new_bomb.position = position
	new_bomb.setCounter(counter)
	new_bomb.setDirection("center")
	new_bomb.explode()
	$AnimationPlayer.play("explode")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "explode":
		queue_free()


func _on_Timer_timeout():
	_explode()
	
func setVelocity(vel):
	cur_velocity = vel
	
func setCounter(c):
	counter = c
