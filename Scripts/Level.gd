extends TileMap


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var breakableWall = preload("res://Scenes/DestroyableBlock.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Pass a 2-dimensional array (1st dimension = y-axis; 2nd = x-axis) to load the level.
# The 2nd dimension contains dictionaries per cell containing a cell type (and name).
func loadData(data):
	for i in get_children():
		i.queue_free()
	clear()
	var tileset = get_tileset()
	var iWall = tileset.find_tile_by_name("wall")
	var len_y = len(data)
	for y in range(len_y):
		for x in range(len(data[y])):
			var dat = data[y][x]
			match dat["type"]:
				"wall":
					set_cell(x,y,iWall)
				"breakable_wall":
					var ins = breakableWall.instance()
					ins.set_position(Vector2(x * get_cell_size().x, y * get_cell_size().y))
					print(str(ins.get_position()) + " " + str(get_cell_size()) + " " + str(x) + "|" + str(y))
					add_child(ins)
