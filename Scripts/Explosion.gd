extends Area2D

# Which type this particular explosion entity is.
# Explosion types determine in which direction the following explosions get spawned.
# Center explosions spawn the following explosions in every direction and set
# their direction.
var type = "center"
var direction = "none"
var explosion_scene = load("res://Scenes/Explosion.tscn")

const player_class = preload("res://Scripts/Player.gd")
const destroyable_class = preload("res://Scripts/DestroyableBlock.gd")
const bomb_class = preload("res://Scripts/Bomb.gd")

# How many explosions are left to follow behind this one
var counter = 0

func setCounter(count):
	counter = count

# If it is not the last explosion to follow, set it as "horizontal" or "vertical".
# This is to determine whether to show the end of an explosion line or the explosion
# line middle.
func setDirection(dir):
	direction = dir
	if counter > 0:
		if direction == "left" or direction == "right":
			type = "horizontal"
		elif direction == "top" or direction == "bottom":
			type = "vertical"
	else:
		type = direction

# Is the collision body the undestroyable level?
func _isLevel(body):
	return body.name.begins_with("Level")
	
func _isEntity(body):
	return (body is player_class) or (body is bomb_class)

# Start the explosion chain.
# This method spawns the next following explosion, sets its counter, etc.
func explode():
	
	# The direction(s) that we should spawn and how far it/they go(es).
	var directions = []
	var counters = []
	
	# Update raycasts as the physics engine couldn't do that in time
	$RayCast2D_bottom.force_raycast_update()
	$RayCast2D_top.force_raycast_update()
	$RayCast2D_left.force_raycast_update()
	$RayCast2D_right.force_raycast_update()
	
	# If this explosion is not the last in the chain
	if counter > 0:
		# Does it not collide with the undestroyable level?
		# TODO: Confusing naming, change it to something like "rightNotColliding"
		var rightCollide = not $RayCast2D_right.is_colliding() or not _isLevel($RayCast2D_right.get_collider())
		var leftCollide = not $RayCast2D_left.is_colliding() or not _isLevel($RayCast2D_left.get_collider())
		var topCollide = not $RayCast2D_top.is_colliding() or not _isLevel($RayCast2D_top.get_collider())
		var bottomCollide = not $RayCast2D_bottom.is_colliding() or not _isLevel($RayCast2D_bottom.get_collider())
		
		# Does it destroy a block?
		var rightDestroys = $RayCast2D_right.is_colliding() and not _isLevel($RayCast2D_right.get_collider()) and not _isEntity($RayCast2D_right.get_collider())
		var leftDestroys = $RayCast2D_left.is_colliding() and not _isLevel($RayCast2D_left.get_collider()) and not _isEntity($RayCast2D_left.get_collider())
		var topDestroys = $RayCast2D_top.is_colliding() and not _isLevel($RayCast2D_top.get_collider()) and not _isEntity($RayCast2D_top.get_collider())
		var bottomDestroys = $RayCast2D_bottom.is_colliding() and not _isLevel($RayCast2D_bottom.get_collider()) and not _isEntity($RayCast2D_bottom.get_collider())
		
		
		# If the following explosion can be spawned (not colliding with level)
		# set spawn to true and if it does destroy a block, set the newcounter
		# to 0 as it will be the last explosion in the chain.
		if direction == "right":
			if rightCollide:
				directions.append("right")
			if rightDestroys:
				counters.append(0)
			else:
				counters.append(counter-1)
		elif direction == "top":
			if topCollide:
				directions.append("top")
			if topDestroys:
				counters.append(0)
			else:
				counters.append(counter-1)
		elif direction == "bottom":
			if bottomCollide:
				directions.append("bottom")
			if bottomDestroys:
				counters.append(0)
			else:
				counters.append(counter-1)
		elif direction == "left":
			if leftCollide:
				directions.append("left")
			if leftDestroys:
				counters.append(0)
			else:
				counters.append(counter-1)
		elif direction == "center":
			# Same thing here except we spawn every direction. (If they can)
			if bottomCollide:
				directions.append("bottom")
				if bottomDestroys:
					counters.append(0)
				else:
					counters.append(counter-1)
			if topCollide:
				directions.append("top")
				if topDestroys:
					counters.append(0)
				else:
					counters.append(counter-1)
			if leftCollide:
				directions.append("left")
				if leftDestroys:
					counters.append(0)
				else:
					counters.append(counter-1)
			if rightCollide:
				directions.append("right")
				if rightDestroys:
					counters.append(0)
				else:
					counters.append(counter-1)
	
	# Spawn the directions with their counter
	for i in range(len(directions)):
		var dir = directions[i]
		var pos = Vector2(position.x, position.y)
		var newc = counters[i]
		if dir == "right":
			pos.x += 16
		elif dir == "left":
			pos.x -= 16
		elif dir == "top":
			pos.y -= 16
		elif dir == "bottom":
			pos.y += 16
		var new_bomb = explosion_scene.instance()
		get_parent().add_child(new_bomb)
		new_bomb.position = pos
		new_bomb.setCounter(newc)
		new_bomb.setDirection(dir)
		new_bomb.explode()
	$AnimationPlayer.play("explosion_" + type)

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name.begins_with("explosion"):
		queue_free()


func _on_Explosion_body_entered(body):
	if body is player_class:
		body.die()
	elif body is destroyable_class:
		body.destroy()
