extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var selected = "WallButton"
var border = preload("res://Scenes/Button_select_border.tscn")
var wall_tex = preload("res://Assets/Sprites/block.png")
var breakable_wall_tex = preload("res://Assets/Sprites/block2.png")
var p1_tex = preload("res://Assets/Sprites/Player_Spawn-1.png")
var p2_tex = preload("res://Assets/Sprites/Player_Spawn-2.png")
var p3_tex = preload("res://Assets/Sprites/Player_Spawn-3.png")
var p4_tex = preload("res://Assets/Sprites/Player_Spawn-4.png")
var preview = null
var scale_x = 1
var scale_y = 1
var grid_x = 16
var grid_y = 16
var level = []
var grid_w = 15
var grid_h = 13

var game_node = null
# Called when the node enters the scene tree for the first time.
func _ready():
	preview = $Preview
	preview.texture = wall_tex
	preview.set_modulate(Color(1, 1, 1, 0.5))
	game_node = get_node("/root/Game")
	print(game_node)
	initialize()
	
func initialize(width=15,height=13):
	grid_w = width
	grid_h = height
	var dimensions = Vector2(width*game_node.get_cell_width(), height*game_node.get_cell_height())
	var dim_window = Vector2(640,ProjectSettings.get_setting("display/window/size/height"))
	scale_x = dim_window.x / dimensions.x
	scale_y = dim_window.y / dimensions.y
	grid_x = game_node.get_cell_width()*scale_x
	grid_y = game_node.get_cell_height()*scale_y
	preview.set_scale(Vector2(scale_x, scale_y))
	var temp = []
	for i in range(height):
		#level.append([])
		temp.append([])
		for j in range(width):
#			level[i].append({
#				"type":"none",
#				"name":""
#			})
			temp[i].append({
				"type":"none",
				"name":""
			})
			if (i == 0 or i == height-1) or (j == 0 or j == width-1):
#				var dup = preview.duplicate()
#				dup.texture = wall_tex
#				dup.position = Vector2(j*grid_x,i*grid_y)
#				dup.set_modulate(Color(1, 1, 1, 1))
#				preview.get_parent().add_child(dup)
#				var name = dup.get_name()
				temp[i][j] = {
					"type":"wall",
					"name":name
				}
	_reload(temp)
	
				
func _setBlock(x, y, type, tex):
	if x > 0 and x < (grid_w-1) and y > 0 and y < (grid_h-1):
		print("placing at " + str(x) + "|" + str(y))
		
		if type.begins_with("player"):
			var finished_purge = false
			for iy in len(level):
				for ix in len(level[iy]):
					if level[iy][ix]["type"] == type:
						level[iy][ix]["type"] = "none"
						get_node(level[iy][ix]["name"]).queue_free()
						level[iy][ix]["name"] = ""
						finished_purge = true
						break
				if finished_purge:
					break
		
		var name = level[y][x]["name"]
		level[y][x]["type"] = type
		if name == "":
			var dup = preview.duplicate()
			dup.texture = tex
			dup.position = Vector2(x*grid_x,y*grid_y)
			dup.set_modulate(Color(1, 1, 1, 1))
			preview.get_parent().add_child(dup)
			var dup_name = dup.get_name()
			level[y][x]["name"] = dup_name
		else:
			get_node(name).texture = tex


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _input(event):
   # Mouse in viewport coordinates
	if event is InputEventMouseButton:
		print("Mouse Click/Unclick at: ", event.position)
		if event.is_pressed() and event.position.x < 640 and not ($SaveDialog.visible):
			print(str(preview.position) + " " + str(grid_x) + "|" + str(grid_y))
			var name = ""
			var tex = null
			if preview.texture == wall_tex:
				name = "wall"
				tex = wall_tex
			elif preview.texture == breakable_wall_tex:
				name = "breakable_wall"
				tex = breakable_wall_tex
			elif preview.texture == p1_tex:
				name = "player1"
				tex = p1_tex
			elif preview.texture == p2_tex:
				name = "player2"
				tex = p2_tex
			elif preview.texture == p3_tex:
				name = "player3"
				tex = p3_tex
			elif preview.texture == p4_tex:
				name = "player4"
				tex = p4_tex
			_setBlock(int(round(preview.position.x/grid_x)),int(round(preview.position.y/grid_y)),name, tex)
	elif event is InputEventMouseMotion:
		if event.position.x < 640:
			#print("Mouse Motion at: ", event.position)
			# Print the size of the viewport
			var x = int(event.position.x / grid_x) * grid_x
			var y = int(event.position.y / grid_y) * grid_y
			preview.position.x = x
			preview.position.y = y
			#print("Viewport Resolution is: ", get_viewport_rect().size)

func _on_WallButton_pressed():
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").get_child(0).queue_free()
	selected = "WallButton"
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").add_child(border.instance())
	preview.texture = wall_tex


func _on_BreakableWallButton_pressed():
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").get_child(0).queue_free()
	selected = "BreakableWallButton"
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").add_child(border.instance())
	preview.texture = breakable_wall_tex


func _on_SaveButton_pressed():
	if not ($SaveDialog.visible or $LoadDialog.visible):
		var dir = Directory.new()
		dir.make_dir_recursive("user://Levels")
		$SaveDialog.popup()

func _reload(data):
	for y in range(level.size()):
		for x in range(level[y].size()):
			if level[y][x]["name"] != "":
				get_node(level[y][x]["name"]).queue_free()
	level = data
	for y in range(grid_h):
		for x in range(grid_w):
			if level[y][x]["type"] != "none":
				var name = level[y][x]["name"]
				var type = level[y][x]["type"]
				var dup = preview.duplicate()
				match type:
					"wall":
						dup.texture = wall_tex
					"breakable_wall":
						dup.texture = breakable_wall_tex
					"player1":
						dup.texture = p1_tex
					"player2":
						dup.texture = p2_tex
					"player3":
						dup.texture = p3_tex
					"player4":
						dup.texture = p4_tex
				
				dup.position = Vector2(x*grid_x,y*grid_y)
				dup.set_modulate(Color(1, 1, 1, 1))
				dup.set_name(name)
				add_child(dup)
				var dup_name = dup.get_name()
				level[y][x]["name"] = dup_name

func _on_LoadButton_pressed():
	if not ($SaveDialog.visible or $LoadDialog.visible):
		var dir = Directory.new()
		dir.make_dir_recursive("user://Levels")
		$LoadDialog.popup()

func _on_SaveDialog_file_selected(path):
	var json = JSON.print(level,"  ",true)
	var file = File.new()
	file.open(path, File.WRITE)
	file.store_string(json)
	print(file.get_path_absolute())
	file.close()


func _on_LoadDialog_file_selected(path):
	var file = File.new()
	file.open(path, File.READ)
	var data = file.get_as_text()
	file.close()
	var dat = JSON.parse(data).get_result()
	_reload(dat)


func _on_ExitButton_pressed():
	get_parent().loadMenu()


func _on_PlayerSpawn1Button_pressed():
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").get_child(0).queue_free()
	selected = "PlayerSpawn1"
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").add_child(border.instance())
	preview.texture = p1_tex


func _on_PlayerSpawn2Button_pressed():
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").get_child(0).queue_free()
	selected = "PlayerSpawn2"
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").add_child(border.instance())
	preview.texture = p2_tex


func _on_PlayerSpawn3Button_pressed():
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").get_child(0).queue_free()
	selected = "PlayerSpawn3"
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").add_child(border.instance())
	preview.texture = p3_tex


func _on_PlayerSpawn4Button_pressed():
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").get_child(0).queue_free()
	selected = "PlayerSpawn4"
	$RightPanel.get_node("ScrollContainer/VBoxContainer/"+selected+"/Button").add_child(border.instance())
	preview.texture = p4_tex
