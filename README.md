# BombermanGD

Bomberman clone made in Godot for testing purposes. Uses free sprites from https://www.spriters-resource.com

# TODO
- [x] Clean up code
- [ ] Clean up assets (e.g. unused sprites inside spritesheets, removing background from sprites)
- [x] Comment everything
- [x] Level Editor
	- [x] Save level data onto file
	- [x] Load the saved level into the editor
	- [x] Access editor through main menu
	- [x] Scrollable list of available blocks to place
- [x] New level system
	- [x] Levels as JSON files (-> Level editor)
	- [x] Send custom levels to players through connection? (or everyone has to have it already..)
	- [x] Level preview has to utilize the JSON system aswell (or use screenshots of levels?)
	- [x] Player spawn coordination are saved per level (current system has fixed points -> levels with variable size possible)
- [ ] Gameplay
	- [x] Upgrades (movement speed, bomb range, etc.)
	- [x] Limit amount of bombs placed at the same time
	- [ ] Conveyor belts? May be a fun feature..
	- [x] Bombs block players
- [ ] Some sort of texture packs
- [ ] Music